/*
 * spi_lib.h
 *
 *  Created on: 17 gru 2015
 *      Author: krystian
 */

#ifndef SPI_LIB_SPI_LIB_H_
#define SPI_LIB_SPI_LIB_H_

#include "ch.h"
#include "hal.h"

/* Exported definitions ---------------------------------------------------- */
/* Exported functions ------------------------------------------------------ */
/**
 * Initialize SPI driver.
 */
void spi_init(void);
void spi_readWrite(uint8_t *rxBuff, const uint8_t *txBuff, uint32_t size);

#endif /* SPI_LIB_SPI_LIB_H_ */
