/*
 * spi_lib.c
 *
 *  Created on: 17 gru 2015
 *      Author: krystian
 */

#include "spi_lib.h"

#define LOG_TAG "spi_lib: "

#include "log.h"

/* Private definitions ----------------------------------------------------- */
#define SPI_DRIVER                  (SPID1)

/* Private variables ------------------------------------------------------- */
SPIConfig config;

/* Exported functions ------------------------------------------------------ */
void spi_init(void)
{
    config.end_cb = NULL;
    config.cr1 = SPI_CR1_BR_2 | SPI_CR1_BR_0;
    config.cr2 = 0;

    // Configure SPI pins
    palSetPadMode(GPIOB, GPIOB_SPI1_SCK, PAL_MODE_ALTERNATE(5));
    palSetPadMode(GPIOB, GPIOB_SPI1_MISO, PAL_MODE_ALTERNATE(5));
    palSetPadMode(GPIOB, GPIOB_SPI1_MOSI, PAL_MODE_ALTERNATE(5));

    spiStart(&SPI_DRIVER, &config);
}

void spi_readWrite(uint8_t *rxBuff, const uint8_t *txBuff, uint32_t size)
{
    spiAcquireBus(&SPI_DRIVER);

    spi_init();

    if(txBuff == NULL && rxBuff == NULL) return;

    if(rxBuff == NULL) {
        spiSend(&SPI_DRIVER, size, txBuff);
    } else if(txBuff == NULL) {
        spiReceive(&SPI_DRIVER, size, rxBuff);
    } else {
        spiExchange(&SPI_DRIVER, size, txBuff, rxBuff);
    }

    spiReleaseBus(&SPI_DRIVER);
}
