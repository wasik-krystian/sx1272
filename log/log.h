/*
 * log.h
 *
 *  Created on: 24 mar 2018
 *      Author: krystian
 */

#ifndef LOG_LOG_H_
#define LOG_LOG_H_

#include "hal.h"
#include "chprintf.h"

#ifndef LOG_TAG
    #define LOG_TAG ""
#endif

#ifndef LOG_LEVEL
    #define LOG_LEVEL 3
#endif

#define LOG_COL_CYAN                    "\033[0;36m"
#define LOG_COL_GREY                    "\033[0;37m"
#define LOG_COL_YELLOW                  "\033[1;33m"
#define LOG_COL_RED                     "\033[1;31m"
#define LOG_COL_DEFAULT                 "\033[0m"

/* Serial driver pointer */
#define LOG_SD_PTR          ((BaseSequentialStream*)&SD2)

#define LOG_ERR(format, ...)        do { if (LOG_LEVEL > 0) chprintf(LOG_SD_PTR, LOG_COL_RED LOG_TAG format LOG_COL_DEFAULT, ##__VA_ARGS__); } while (0)
#define LOG(format, ...)            do { if (LOG_LEVEL > 1) chprintf(LOG_SD_PTR, LOG_COL_CYAN LOG_TAG format LOG_COL_DEFAULT, ##__VA_ARGS__); } while (0)
#define LOG_WRN(format, ...)        do { if (LOG_LEVEL > 2) chprintf(LOG_SD_PTR, LOG_COL_YELLOW LOG_TAG format LOG_COL_DEFAULT, ##__VA_ARGS__); } while (0)
#define LOG_DBG(format, ...)        do { if (LOG_LEVEL > 3) chprintf(LOG_SD_PTR, LOG_COL_GREY LOG_TAG format LOG_COL_DEFAULT, ##__VA_ARGS__); } while (0)

#endif /* LOG_LOG_H_ */
