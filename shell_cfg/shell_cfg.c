/*
 * shellcfg.c
 *
 *  Created on: 14 paź 2017
 *      Author: krystian
 */

#include "shell_cfg.h"

#include <string.h>
#include <stdlib.h>

#define LOG_TAG "shell: "
#define LOG_LEVEL 4

#include "log.h"
#include "sx1272.h"
#include "transmitter.h"

static char shell_history_buffer[SHELL_MAX_HIST_BUFF];
static char *shell_completion_buffer[SHELL_MAX_COMPLETIONS];

static void cmd_sx1272_transmit(BaseSequentialStream *chp, int argc, char *argv[])
{
    (void)chp;
    (void)argc;
    (void)argv;

    if (argc == 0) {
        LOG_ERR("Please specify command (on/off/single)\n\r");

    } else {
        if (strcmp(argv[0], "on") == 0) {
            if (argc < 3 || argc > 3) {
                LOG_ERR("Usage: tx on <pn_variant> <period_s>\n\r");
                return;
            }
            unsigned pn_variant = atoi(argv[1]);
            unsigned period_ms = atoi(argv[2]) * 1000;
            transmitter_start(pn_variant, period_ms);
        } else if (strcmp(argv[0], "off") == 0) {
            if (argc == 1) {
                transmitter_stop();
            } else {
                LOG_ERR("Too many arguments\n\r");
            }
        } else if (strcmp(argv[0], "single") == 0) {
            if (argc == 1) {
                transmitter_single();
            } else {
                LOG_ERR("Too many arguments\n\r");
            }
        } else if (strcmp(argv[0], "delay") == 0) {
            if (argc < 2 || argc > 2) {
                LOG_ERR("Usage: tx delay <delay_ms>\n\r");
                return;
            }
            unsigned delay_ms = atoi(argv[1]);
            transmitter_delay(delay_ms);
        } else {
            LOG_ERR("Unknown command\n\r");
        }
    }
}

static void cmd_sx1272_power(BaseSequentialStream *chp, int argc, char *argv[])
{
    (void)chp;
    (void)argc;
    (void)argv;

    int res;

    if (argc == 0) { /* Get */
        LOG("Current TX power: %u\n\r", sx1272_getTXPower());

    } else if (argc == 1) { /* Set */
        unsigned power = atoi(argv[0]);
        LOG("Setting TX power to: %u\n\r", power);
        if ((res = sx1272_setTXPower(power)) < 0)
            LOG_ERR("Failed to set TX power (%d)\n\r", res);

    } else {
        LOG_ERR("Too many parameters\n\r");
    }
}

static void cmd_sx1272_bitrate(BaseSequentialStream *chp, int argc, char *argv[])
{
    (void)chp;
    (void)argc;
    (void)argv;

    int res;

    if (argc == 0) { /* Get */
        LOG("Current bitrate: %u\n\r", sx1272_getBitRate());

    } else if (argc == 1) { /* Set */
        unsigned bitrate = atoi(argv[0]);
        LOG("Setting bitrate to: %u\n\r", bitrate);
        if ((res = sx1272_setBitRate(bitrate)) < 0)
            LOG_ERR("Failed to set bitrate (%d)\n\r", res);

    } else {
        LOG_ERR("Too many parameters\n\r");
    }
}

static void cmd_sx1272_fdev(BaseSequentialStream *chp, int argc, char *argv[])
{
    (void)chp;
    (void)argc;
    (void)argv;

    int res;

    if (argc == 0) { /* Get */
        LOG("Current frequency deviation: %u\n\r", sx1272_getFreqDev());

    } else if (argc == 1) { /* Set */
        unsigned fdev = atoi(argv[0]);
        LOG("Setting frequency deviation to: %u\n\r", fdev);
        if ((res = sx1272_setFreqDev(fdev)) < 0)
            LOG_ERR("Failed to set frequency deviation (%d)\n\r", res);

    } else {
        LOG_ERR("Too many parameters\n\r");
    }
}

static const ShellCommand shell_commands[] = {
        {"tx", cmd_sx1272_transmit},
        {"power", cmd_sx1272_power},
        {"bitrate", cmd_sx1272_bitrate},
        {"fdev", cmd_sx1272_fdev},
        {NULL, NULL},
};

static ShellConfig shell_cfg = {
        NULL,
        shell_commands,
        shell_history_buffer,
        sizeof(shell_history_buffer),
        shell_completion_buffer
};

static thread_t *shell_thd_ptr = NULL;


void shell_init(BaseSequentialStream *stream) {
    if (!stream) return;

    shell_cfg.sc_channel = stream;

    shell_thd_ptr = chThdCreateFromHeap(NULL, SHELL_WA_SIZE, "shell",
            NORMALPRIO - 1, shellThread, (void*)&shell_cfg);
}
