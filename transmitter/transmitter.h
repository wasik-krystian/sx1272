/*
 * transmitter.h
 *
 *  Created on: 3 cze 2018
 *      Author: krystian
 */

#ifndef TRANSMITTER_TRANSMITTER_H_
#define TRANSMITTER_TRANSMITTER_H_

void transmitter_init(void);
int transmitter_start(int pn_variant, uint16_t period_ms);
int transmitter_stop(void);
int transmitter_single(void);
void transmitter_delay(uint32_t ms);

#endif /* TRANSMITTER_TRANSMITTER_H_ */
