/*
 * thread.c
 *
 *  Created on: 3 cze 2018
 *      Author: krystian
 */

#include <ch.h>
#include <string.h>

#include "sx1272.h"
#include "lfsr.h"

#define LOG_TAG "transmitter: "

#include "log.h"

#define TRANSMITTER_EVT_START           (1 << 0)
#define TRANSMITTER_EVT_STOP            (1 << 1)
#define TRANSMITTER_EVT_SINGLE          (1 << 2)

#define TX_LENGTH                       (256)

static int _started = 0;

static volatile uint32_t _period_ms = 0;
static volatile int _pn_variant = 0;
static volatile unsigned _delay_ms = 0;

static lfsr_t _lfsr;

static thread_t *_threadPtr = NULL;

static event_source_t _eventSource;

static THD_WORKING_AREA(_wa, 0x1000);

static void transmitter_transmitNow(void)
{
    LOG("Transmitting data (%u bytes, sequence %u)...\n\r", TX_LENGTH, _pn_variant);

    uint16_t i;
    uint8_t data[TX_LENGTH];

    chprintf(LOG_SD_PTR, "[ ");
    for (i = 0; i < TX_LENGTH; i++) {
        if (_pn_variant) {
            data[i] = lfsr_generate(&_lfsr);
        } else {
            data[i] = (i >= (TX_LENGTH/2)) ? (0xff) : (0x0);
        }
        chprintf(LOG_SD_PTR, "0x%02x ", data[i]);
        if ((i+1)%16 == 0 && (i+1) != TX_LENGTH)
            chprintf(LOG_SD_PTR, "\n\r  ");
    }
    chprintf(LOG_SD_PTR, "]\n\r");

    sx1272_transmit(data, TX_LENGTH);
}

static THD_FUNCTION(transmitter_thread, arg)
{
    (void)arg;
    eventmask_t eventmask;

    chEvtObjectInit(&_eventSource);
    chRegSetThreadName("transmitter");

    systime_t next_tx = 0;
    while (true) {
        eventmask = chEvtWaitAnyTimeout(ALL_EVENTS, MS2ST(10));

        /* Process events */
        if (eventmask & TRANSMITTER_EVT_START) {
            _started = 1;
            next_tx = 0;
            LOG("Transmitter started\n\r");
        }
        if (eventmask & TRANSMITTER_EVT_STOP) {
            _started = 0;
            LOG("Transmitter stopped\n\r");
        }
        if (eventmask & TRANSMITTER_EVT_SINGLE)
            transmitter_transmitNow();

        /* Transmit */
        systime_t now = chVTGetSystemTime();
        if (_started && next_tx < now) {
            next_tx = now + MS2ST(_period_ms);

            /* Apply delay once */
            if (_delay_ms) {
                LOG_WRN("Delaying periodic transmission by %u ms\n\r", _delay_ms);
                next_tx += MS2ST(_delay_ms);
                _delay_ms = 0;
            }

            transmitter_transmitNow();
        }
    }
}

void transmitter_init(void)
{
    _threadPtr = chThdCreateStatic(_wa, sizeof(_wa), NORMALPRIO + 2, transmitter_thread, NULL);

    lfsr_init(&_lfsr, 1);
}

int transmitter_start(int pn_variant, uint16_t period_ms)
{
    if (_threadPtr == NULL) {
        LOG_ERR("Not initialized yet\n\r");
        return -1;
    }
    if (_started) {
        LOG_ERR("Already started\n\r");
        return -1;
    }

    /* TODO: Add mutex to guard those values */
    _period_ms = period_ms;
    _pn_variant = pn_variant;

    if (pn_variant)
        lfsr_init(&_lfsr, pn_variant);

    chEvtSignal(_threadPtr, TRANSMITTER_EVT_START);

    return 0;
}

int transmitter_stop(void)
{
    if (_threadPtr == NULL) {
        LOG_ERR("Not initialized yet\n\r");
        return -1;
    }
    if (!_started) {
        LOG_ERR("Already stopped\n\r");
        return -1;
    }

    chEvtSignal(_threadPtr, TRANSMITTER_EVT_STOP);

    return 0;
}

int transmitter_single(void)
{
    if (_threadPtr == NULL) {
        LOG_ERR("Not initialized yet\n\r");
        return -1;
    }
    if (_started) {
        LOG_ERR("Already started\n\r");
        return -1;
    }

    chEvtSignal(_threadPtr, TRANSMITTER_EVT_SINGLE);

    return 0;
}

void transmitter_delay(uint32_t ms)
{
    _delay_ms = ms;
}
