/*
 * lfsr.h
 *
 *  Created on: 3 cze 2018
 *      Author: krystian
 */

#ifndef TRANSMITTER_LFSR_H_
#define TRANSMITTER_LFSR_H_

#include <stdint.h>

typedef struct {
    uint16_t state;
} lfsr_t;

void lfsr_init(lfsr_t *lfsr, int pn_variant);
uint8_t lfsr_generate(lfsr_t *lfsr);

#endif /* TRANSMITTER_LFSR_H_ */
