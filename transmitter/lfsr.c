/*
 * lfsr.c
 *
 *  Created on: 3 cze 2018
 *      Author: krystian
 */

#include "lfsr.h"

#define LOG_TAG "lfsr: "

#include "log.h"

#include <stddef.h>

void lfsr_init(lfsr_t *lfsr, int pn_variant)
{
    uint16_t initial_state = 0xace1;

    if (pn_variant == 1) {
        initial_state = 0xbf5a;
    } else if (pn_variant != 2) {
        LOG_WRN("Unknown pseudo-noise sequence number\n\r");
        LOG_WRN("Using default initial state (0x%x)\n\r", initial_state);
    }

    if (lfsr != NULL)
        lfsr->state =initial_state;
}

uint8_t lfsr_generate(lfsr_t *lfsr)
{
    uint16_t i;
    uint16_t state = lfsr->state;
    uint16_t bit;

    for (i  = 0; i < 8; i++) {
        /* taps: 16 14 13 11; feedback polynomial: x^16 + x^14 + x^13 + x^11 + 1 */
        bit  = ((state >> 0) ^ (state >> 2) ^ (state >> 3) ^ (state >> 5) ) & 1;
        state =  (state >> 1) | (bit << 15);
    }

    lfsr->state = state;

    return state;
}
