/*
 * sx1272-bsp.h
 *
 *  Created on: 18 lut 2018
 *      Author: krystian
 */

#ifndef RADIO_SX1272_HAL_SX1272_BSP_H_
#define RADIO_SX1272_HAL_SX1272_BSP_H_

#include <sx1272-types.h>

/* TODO: Pin assignment */
#define RADIO_NSS                       ((Gpio_t){0, 0})
#define RADIO_RESET                     ((Gpio_t){0, 0})
#define RADIO_DIO_0                     ((Gpio_t){0, 0})
#define RADIO_DIO_1                     ((Gpio_t){0, 0})
#define RADIO_DIO_2                     ((Gpio_t){0, 0})
#define RADIO_DIO_3                     ((Gpio_t){0, 0})
#define RADIO_DIO_4                     ((Gpio_t){0, 0})
#define RADIO_DIO_5                     ((Gpio_t){0, 0})
#define RADIO_ANT_SWITCH_TX             ((Gpio_t){0, 0})
#define RADIO_ANT_SWITCH_RX             ((Gpio_t){0, 0})

#endif /* RADIO_SX1272_HAL_SX1272_BSP_H_ */
