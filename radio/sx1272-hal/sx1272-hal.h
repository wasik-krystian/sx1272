/*
 * hal.h
 *
 *  Created on: 18 lut 2018
 *      Author: krystian
 */

#ifndef RADIO_SX1272_HAL_SX1272_HAL_H_
#define RADIO_SX1272_HAL_SX1272_HAL_H_

#include <sx1272-bsp.h>

void GpioInit(Gpio_t *g,
              Gpio_t init,
              uint8_t mode,
              uint8_t type,
              uint8_t pull,
              uint8_t state);
void GpioWrite(Gpio_t *g, uint8_t state);
void GpioSetInterrupt(Gpio_t *g,
                      uint8_t edge,
                      uint8_t priority,
                      void (*handler)(void));

uint8_t SpiInOut(Spi_t *s, uint8_t in);

void TimerInit(TimerEvent_t *t, void (*clbk)(void));
void TimerSetValue(TimerEvent_t *t, uint32_t value);
void TimerStart(TimerEvent_t *t);
void TimerStop(TimerEvent_t *t);

void DelayMs(uint32_t ms);

#endif /* RADIO_SX1272_HAL_SX1272_HAL_H_ */
