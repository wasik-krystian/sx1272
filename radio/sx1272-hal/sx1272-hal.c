/*
 * hal.c
 *
 *  Created on: 18 lut 2018
 *      Author: krystian
 */

#include <sx1272-hal.h>
#include "spi_lib.h"

/* TODO: Implement sx1272 HAL */

void GpioInit(Gpio_t *g,
              Gpio_t init,
              uint8_t mode,
              uint8_t type,
              uint8_t pull,
              uint8_t state)
{

}

void GpioWrite(Gpio_t *g, uint8_t state)
{

}

void GpioSetInterrupt(Gpio_t *g,
                      uint8_t edge,
                      uint8_t priority,
                      void (*handler)(void))
{

}

uint8_t SpiInOut(Spi_t *s, uint8_t in)
{
    uint8_t out;
    spi_readWrite(&out, &in, 1);
    return out;
}

void TimerInit(TimerEvent_t *t, void (*clbk)(void))
{

}

void TimerSetValue(TimerEvent_t *t, uint32_t value)
{

}

void TimerStart(TimerEvent_t *t)
{

}

void TimerStop(TimerEvent_t *t)
{

}

void DelayMs(uint32_t ms)
{

}
