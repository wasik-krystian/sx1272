/*
 * sx1272-types.h
 *
 *  Created on: 18 lut 2018
 *      Author: krystian
 */

#ifndef RADIO_SX1272_HAL_SX1272_TYPES_H_
#define RADIO_SX1272_HAL_SX1272_TYPES_H_

#include <stdint.h>

#define PIN_OUTPUT                      (0)
#define PIN_INPUT                       (1)
#define PIN_ANALOGIC                    (2)

#define PIN_PUSH_PULL                   (0)

#define PIN_NO_PULL                     (0)
#define PIN_PULL_UP                     (1)

#define IRQ_RISING_EDGE                 (0)

#define IRQ_HIGH_PRIORITY               (0)

typedef struct {
    uint32_t port;
    uint32_t pin;
} Gpio_t;

typedef struct {
    Gpio_t Nss;
} Spi_t;

typedef struct {

} TimerEvent_t;

#endif /* RADIO_SX1272_HAL_SX1272_TYPES_H_ */
