/*
 * sx1272.h
 *
 *  Created on: 24 mar 2018
 *      Author: krystian
 */

#ifndef RADIO_SX1272_SX1272_H_
#define RADIO_SX1272_SX1272_H_

#include "sx1272_regs.h"

typedef enum {
    sx1272_mode__sleep  = 0b000,
    sx1272_mode__stdby  = 0b001,
    sx1272_mode__FSTx   = 0b010,
    sx1272_mode__Tx     = 0b011,
    sx1272_mode__FSRx   = 0b100,
    sx1272_mode__Rx     = 0b101,
} sx1272_mode_t;

int sx1272_init(void);

void sx1272_write(uint8_t addr, const uint8_t *buffer, uint8_t size);
void sx1272_read(uint8_t addr, uint8_t *buffer, uint8_t size);

int sx1272_writeReg(uint8_t regAddr, uint8_t val, int verify);
uint8_t sx1272_readReg(uint8_t regAddr);

void sx1272_reset(void);

void sx1272_setMode(sx1272_mode_t mode);

void sx1272_transmit(const uint8_t *data, size_t len);

void sx1272_printStatus(void);

unsigned sx1272_getTXPower(void);
int sx1272_setTXPower(unsigned power);

unsigned sx1272_getBitRate(void);
int sx1272_setBitRate(unsigned bitrate);

unsigned sx1272_getFreqDev(void);
int sx1272_setFreqDev(unsigned fdev);

#endif /* RADIO_SX1272_SX1272_H_ */
