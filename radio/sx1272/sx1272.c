/*
 * sx1272.c
 *
 *  Created on: 24 mar 2018
 *      Author: krystian
 */

#include "spi_lib.h"
#include "chprintf.h"
#include "sx1272.h"

#define LOG_TAG "sx1272: "

// 4 - with debug logs
#define LOG_LEVEL 3

#include "log.h"

#include <string.h>

#define SX1272_CHIP_SELECT()            palClearPad(GPIOA, GPIOA_SX1272_NSEL)
#define SX1272_CHIP_DESELECT()          palSetPad(GPIOA, GPIOA_SX1272_NSEL)

#define SX1276

#ifdef SX1276
    #define EXPECTED_VERSION            (0x12)
#else
    #define EXPECTED_VERSION            (0x22)
#endif

#define SX127x_FSTEP                    (61) /* Hz */
#define SX127x_FXOSC                    (32*1000*1000)  /* Hz */

#define SX127x_MIN_POWER                (2)
#define SX127x_MAX_POWER                (17)

#define SX127x_MIN_FREQ_DEV             (10 * SX127x_FSTEP)
#define SX127x_MAX_FREQ_DEV             (3278 * SX127x_FSTEP)

#define SX127x_DEFAULT_FREQ_DEV         (100000)

#define SX127x_MIN_BIT_RATE             (1200)
#define SX127x_MAX_BIT_RATE             (300000)

#define SX127x_DEFAULT_BIT_RATE         (SX127x_MIN_BIT_RATE * 8)

static unsigned sx1272_TXPower = SX127x_MAX_POWER; /* dBm */
static unsigned sx1272_FreqDev = SX127x_DEFAULT_FREQ_DEV; /* Hz */
static unsigned sx1272_BitRate = SX127x_DEFAULT_BIT_RATE; /* bits/s */

int sx1272_init(void)
{
    uint8_t val;

    sx1272_reset();

    uint8_t version = sx1272_readReg(REG_VERSION);
    LOG("RegVersion = 0x%x\n\r", version);
    if (version != EXPECTED_VERSION) {
        LOG_ERR("Version (0x%x) other than expected (0x%x)\n\r", version, EXPECTED_VERSION);
        return -1;
    }

    LOG("Setting REG_SYNCCONFIG...\n\r");
#ifdef USE_SYNC_SEQUENCE
    val = 0 | RF_SYNCCONFIG_SYNC_ON
            | RF_SYNCCONFIG_SYNCSIZE_4;
#else
    val = 0 | RF_SYNCCONFIG_SYNC_OFF;
#endif
    if (sx1272_writeReg(REG_SYNCCONFIG, val, 1))
        return -1;

#ifdef SX1276
    if (sx1272_setTXPower(sx1272_TXPower)) return -1;
    if (sx1272_setBitRate(sx1272_BitRate)) return -1;
    if (sx1272_setFreqDev(sx1272_FreqDev)) return -1;

#if 0
    LOG("Setting REG_PADAC...\n\r");
    if (sx1272_writeReg(REG_PADAC, 0x87, 1)) return -1;
#endif
#endif

#ifdef USE_SYNC_SEQUENCE
    LOG("Setting sync value...\n\r");
    if (sx1272_writeReg(REG_SYNCVALUE1, 0xDE, 1)) return -1;
    if (sx1272_writeReg(REG_SYNCVALUE2, 0xAD, 1)) return -1;
    if (sx1272_writeReg(REG_SYNCVALUE3, 0xBE, 1)) return -1;
    if (sx1272_writeReg(REG_SYNCVALUE4, 0xEF, 1)) return -1;
#endif

    /* Unlimited packet format */
    LOG("Setting unlimited packet format...\n\r");
    if (sx1272_writeReg(REG_PACKETCONFIG1, RF_PACKETCONFIG1_DCFREE_OFF, 1)) return -1;
    if (sx1272_writeReg(REG_PAYLOADLENGTH, 0x0, 1)) return -1;

    /* RF Carrier Frequency Frf = 868.0 MHz */
    LOG("Setting RF carrier frequency...\n\r");
    if (sx1272_writeReg(REG_FRFMSB, 0xD9, 1)) return -1;
    if (sx1272_writeReg(REG_FRFMID, 0x00, 1)) return -1;
    if (sx1272_writeReg(REG_FRFLSB, 0x00, 1)) return -1;

    LOG("Reading RegOpMode...\n\r");
    if (sx1272_writeReg(REG_OPMODE, RF_OPMODE_MODULATIONTYPE_FSK, 1)) return -1;

    LOG("Go into stand by...\n\r");
    sx1272_setMode(sx1272_mode__stdby);

    return 0;
}

void sx1272_write(uint8_t addr, const uint8_t *buffer, uint8_t size)
{
    uint8_t tx_buf[size + 1];

    tx_buf[0] = addr |= 0x80;
    memcpy(tx_buf + 1, buffer, size);

    SX1272_CHIP_SELECT();
    spi_readWrite(NULL, tx_buf, size + 1);
    SX1272_CHIP_DESELECT();
}

void sx1272_read(uint8_t addr, uint8_t *buffer, uint8_t size)
{
    uint8_t tx_buf[size + 1];
    uint8_t rx_buf[size + 1];

    tx_buf[0] = addr & ~(0x80);
    memset(tx_buf + 1, 0, size);

    SX1272_CHIP_SELECT();
    spi_readWrite(rx_buf, tx_buf, size + 1);
    SX1272_CHIP_DESELECT();

    memcpy(buffer, rx_buf + 1, size);
}

int sx1272_writeReg(uint8_t regAddr, uint8_t val, int verify)
{
    LOG_DBG("sx1272_writeReg(0x%x, 0x%x)\n\r", regAddr, val);
    sx1272_write(regAddr, &val, 1);
#if 0
    if (verify && val != sx1272_readReg(regAddr)) {
        LOG_ERR("Verification failed.\n\r", regAddr, val);
        return -1;
    }
#else
    (void)verify;
#endif

    return 0;
}

uint8_t sx1272_readReg(uint8_t regAddr)
{
    uint8_t val;
    sx1272_read(regAddr, &val, 1);

    /* WARNING! Enabling this log messes up transmitting packets longer than
     *          FIFO length, due to timing issues. */
#if 0
    LOG_DBG("sx1272_readRed(0x%x) = 0x%x\n\r", regAddr, val);
#endif

    return val;
}

#ifdef SX1276
void sx1272_reset(void)
{
    /* Reset pulse */
    palSetPadMode(GPIOA, GPIOA_SX1272_RESET, PAL_MODE_OUTPUT_PUSHPULL);
    palClearPad(GPIOA, GPIOA_SX1272_RESET);
    chThdSleepMilliseconds(20);
    palSetPad(GPIOA, GPIOA_SX1272_RESET);

    /* Wait at least 10ms */
    chThdSleepMilliseconds(20);
}
#else
void sx1272_reset(void)
{
    /* High-Z state */
    palSetPadMode(GPIOA, GPIOA_SX1272_RESET, PAL_MODE_INPUT);

    chThdSleepMilliseconds(1);

    /* Reset pulse */
    palSetPadMode(GPIOA, GPIOA_SX1272_RESET, PAL_MODE_OUTPUT_PUSHPULL);
    palSetPad(GPIOA, GPIOA_SX1272_RESET);
    chThdSleepMilliseconds(1);
    palClearPad(GPIOA, GPIOA_SX1272_RESET);

    /* High-Z */
    palSetPadMode(GPIOA, GPIOA_SX1272_RESET, PAL_MODE_INPUT);

    /* Wait at least 10ms */
    chThdSleepMilliseconds(20);
}
#endif

#define OPMODE_MASK     (0b111)

void sx1272_setMode(sx1272_mode_t mode)
{
    uint8_t val = sx1272_readReg(REG_OPMODE);
    val &= ~OPMODE_MASK;
    val |= mode;
    sx1272_writeReg(REG_OPMODE, val, 1);
}

int sx1272_startTopLevelSequencer(void)
{
    uint8_t val;

    val = RF_SEQCONFIG1_SEQUENCER_START
        | RF_SEQCONFIG1_IDLEMODE_STANDBY
        | RF_SEQCONFIG1_FROMSTART_TOTX_ONFIFOLEVEL
        | RF_SEQCONFIG1_LPS_SEQUENCER_OFF
        | RF_SEQCONFIG1_FROMTX_TOLPS;

    return sx1272_writeReg(REG_SEQCONFIG1, val, 1);
}

#define MAX_PACKET_LENGTH           (2047)
#define FIFO_LENGTH                 (64)

void sx1272_transmit(const uint8_t *data, size_t len)
{
    int res;
    systime_t start;

    if (len > MAX_PACKET_LENGTH) {
        LOG_ERR("Payload too big. Dropping\n\r");
        return;
    }

    /* Set MSB of packet length */
    uint8_t val = sx1272_readReg(REG_PACKETCONFIG2);
    val &= RF_PACKETCONFIG2_PAYLOADLENGTH_MSB_MASK;
    val |= (len >> 8) & (~RF_PACKETCONFIG2_PAYLOADLENGTH_MSB_MASK);
    res = sx1272_writeReg(REG_PACKETCONFIG2, val, 1);
    if (res < 0) {
        LOG_ERR("Failed to set packet length (MSB)\n\r");
        return;
    }

    /* Set LSB of packet length */
    res = sx1272_writeReg(REG_PAYLOADLENGTH, len, 1);
    if (res < 0) {
        LOG_ERR("Failed to set packet length (LSB)\n\r");
        return;
    }

    start = chVTGetSystemTime();

    /* Start Top Level Sequencer */
    LOG_DBG("Starting Top Level Sequencer...\n\r");
    sx1272_startTopLevelSequencer();

    if (len <= FIFO_LENGTH) {
        LOG_DBG("Transmitting whole packet right away...\n\r");
        sx1272_write(REG_FIFO, data, len);

    } else {
        LOG_DBG("Transmitting. Will have to split up...\n\r");

        /* Fill up FIFO */
        sx1272_write(REG_FIFO, data, FIFO_LENGTH);
        data += FIFO_LENGTH;
        len -= FIFO_LENGTH;

        /* Send byte by byte as space becomes available */
        while (len) {
            while (sx1272_readReg(REG_IRQFLAGS2) & RF_IRQFLAGS2_FIFOFULL);
            sx1272_write(REG_FIFO, data, 1);
            data++;
            len--;
        }
    }

    /* Wait until FIFO is emptied */
    while (!(sx1272_readReg(REG_IRQFLAGS2) & RF_IRQFLAGS2_FIFOEMPTY));

    LOG("Transmission took %u us\n\r", ST2US(chVTGetSystemTime() - start));
}

void sx1272_printStatus(void)
{
    uint8_t val;

    val = sx1272_readReg(REG_IRQFLAGS1);
    LOG("RegIrqFlags1 = 0x%x\n\r", val);
    if (val & RF_IRQFLAGS1_MODEREADY)           LOG("\tMODEREADY\n\r");
    if (val & RF_IRQFLAGS1_PLLLOCK)             LOG("\tPLLLOCK\n\r");
    if (val & RF_IRQFLAGS1_PREAMBLEDETECT)      LOG("\tPREAMBLEDETECT\n\r");
    if (val & RF_IRQFLAGS1_RSSI)                LOG("\tRSSI\n\r");
    if (val & RF_IRQFLAGS1_RXREADY)             LOG("\tRXREADY\n\r");
    if (val & RF_IRQFLAGS1_SYNCADDRESSMATCH)    LOG("\tSYNCADDRESSMATCH\n\r");
    if (val & RF_IRQFLAGS1_TIMEOUT)             LOG("\tTIMEOUT\n\r");
    if (val & RF_IRQFLAGS1_TXREADY)             LOG("\tTXREADY\n\r");

    val = sx1272_readReg(REG_IRQFLAGS2);
    LOG("RegIrqFlags2 = 0x%x\n\r", val);
    if (val & RF_IRQFLAGS2_CRCOK) LOG("\tCRCOK\n\r");
    if (val & RF_IRQFLAGS2_FIFOEMPTY) LOG("\tFIFOEMPTY\n\r");
    if (val & RF_IRQFLAGS2_FIFOFULL) LOG("\tFIFOFULL\n\r");
    if (val & RF_IRQFLAGS2_FIFOLEVEL) LOG("\tFIFOLEVEL\n\r");
    if (val & RF_IRQFLAGS2_FIFOOVERRUN) LOG("\tFIFOOVERRUN\n\r");
    if (val & RF_IRQFLAGS2_LOWBAT) LOG("\tLOWBAT\n\r");
    if (val & RF_IRQFLAGS2_PACKETSENT) LOG("\tPACKETSENT\n\r");
    if (val & RF_IRQFLAGS2_PAYLOADREADY) LOG("\tPAYLOADREADY\n\r");
}

unsigned sx1272_getTXPower(void)
{
    return sx1272_TXPower;
}

int sx1272_setTXPower(unsigned power)
{
    LOG("Setting TX power to %u dBm\n\r", power);

    if (power > SX127x_MAX_POWER || power < SX127x_MIN_POWER) {
        LOG_ERR("Unsupported TX power (min=%u, max=%u)\n\r", SX127x_MIN_POWER, SX127x_MAX_POWER);
        return -1;
    }

    uint8_t val = RF_PACONFIG_PASELECT_PABOOST | (power - 2);

    if (sx1272_writeReg(REG_PACONFIG, val, 1) < 0) return -1;

    sx1272_TXPower = power;

    return 0;
}

unsigned sx1272_getBitRate(void)
{
    return sx1272_BitRate;
}

int sx1272_setBitRate(unsigned bitrate)
{
    unsigned n = SX127x_FXOSC/bitrate;

    if (bitrate < SX127x_MIN_BIT_RATE || bitrate > SX127x_MAX_BIT_RATE) {
        LOG_ERR("Unsupported bit rate (min=%u, max=%u)\n\r", SX127x_MIN_BIT_RATE, SX127x_MAX_BIT_RATE);
        return -1;
    }

    LOG("Setting bit rate to %u bits per second\n\r", SX127x_FXOSC/n);

    if (sx1272_writeReg(REG_BITRATEMSB, (n >> 8) & 0xff, 1) < 0) return -1;
    if (sx1272_writeReg(REG_BITRATELSB, n & 0xff, 1) < 0) return -1;

    sx1272_BitRate = SX127x_FXOSC/n;

    return 0;
}

unsigned sx1272_getFreqDev(void)
{
    return sx1272_FreqDev;
}

int sx1272_setFreqDev(unsigned fdev)
{
    unsigned n = fdev/SX127x_FSTEP;

    LOG("Setting frequency deviation to %u Hz\n\r", n*SX127x_FSTEP);

    if (n*SX127x_FSTEP > SX127x_MAX_FREQ_DEV) {
        n = SX127x_MAX_FREQ_DEV/SX127x_FSTEP;
        LOG_WRN("Frequency deviation too high. Setting to %u instead\n\r", n*SX127x_FSTEP);
    }

    if (n*SX127x_FSTEP < SX127x_MIN_FREQ_DEV) {
        n = SX127x_MIN_FREQ_DEV/SX127x_FSTEP;
        LOG_WRN("Frequency deviation too low. Setting to %u instead\n\r", n*SX127x_FSTEP);
    }

    if (sx1272_writeReg(REG_FDEVMSB, (n >> 8) & 0xff, 1) < 0) return -1;
    if (sx1272_writeReg(REG_FDEVLSB, n & 0xff, 1) < 0) return -1;

    sx1272_FreqDev = n*SX127x_FSTEP;

    return 0;
}
