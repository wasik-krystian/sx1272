/*
    ChibiOS - Copyright (C) 2006..2016 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "ch.h"
#include "hal.h"
#include "ch_test.h"
#include "shell_cfg.h"
#include "spi_lib.h"

#include "sx1272.h"
#include "transmitter.h"

#define LOG_TAG "main: "

#include "log.h"

#include <string.h>

/*
 * Green LED blinker thread, times are in milliseconds.
 */
static THD_WORKING_AREA(waThread1, 0x1000);
static THD_FUNCTION(Thread1, arg) {

  (void)arg;
  chRegSetThreadName("blinker");
  while (true) {
    palClearPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(1000);
    palSetPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(100);
  }
}

/*
 * Application entry point.
 */
int main(void) {
    unsigned i;

    /*
     * System initializations.
     * - HAL initialization, this also initializes the configured device drivers
     *   and performs the board-specific initializations.
     * - Kernel initialization, the main() function becomes a thread and the
     *   RTOS is active.
     */
    halInit();
    chSysInit();

    /*
     * Activates the serial driver 2 using the driver default configuration.
     */
    sdStart(&SD2, NULL);

    for (i = 0; i < 20; i++)
        chprintf(LOG_SD_PTR, "\n\r");

    chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO + 1, Thread1, NULL);

    shell_init((BaseSequentialStream*)&SD2);

    chThdSleep(MS2ST(1000));

    LOG("Initializing SPI...\n\r");
    spi_init();

    LOG("Initializing SX1272...\n\r");
    if (sx1272_init() < 0) {
        LOG_ERR("Failed to initialize SX1272\n\r");
        goto fail;
    }

    LOG("Initializing transmitter module...\n\r");
    transmitter_init();

    chThdSleepMilliseconds(500);

    int curr_state, prev_state = 1;
    while (true) {
        curr_state = palReadPad(GPIOC, GPIOC_BUTTON);
        if (curr_state == 0 && prev_state == 1)
            transmitter_start(2, 6000);

        prev_state = curr_state;

        chThdSleepMilliseconds(100);
    }

fail:
    for (;;)
        chThdSleepMilliseconds(1000);
}
